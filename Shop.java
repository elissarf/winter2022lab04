import java.util.Scanner;

/*
 this is a subway shop, i will ask some basic ingredients they want, 
 then tell them what they ordered, the price and tip
 */
public class Shop {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Kitchen[] k = new Kitchen[4];

        System.out.println("Hi, welcome to subway the four of you, well let's start...  ");

        for (int i = 0; i < k.length - 1; i++) {
            System.out.println(" ");

            System.out.println("customer number " + (i + 1) + ", for bread, do you want white or italien?");
            String bread = scan.next();

            System.out.println("Do you want chicken, meatball or lamb?");
            // never saw vegeterian subway so not gonna bother with that
            String protein = scan.next();

            System.out.println("Do you want vegetables? yes or no");
            char veggy = Character.toLowerCase(scan.next().charAt(0));

            System.out.println("Do you want cookies? yes or no");
            char cookieOrNot = Character.toLowerCase(scan.next().charAt(0));
            int cookies = 0;
            if (cookieOrNot == 'y') {
                System.out.println("how many cookies do you want?");
                cookies = scan.nextInt();
            }

            k[i] = new Kitchen(bread, protein, veggy, cookies);
        }

        /*
         * this is a new item in array, so i use a different constructer to make it look
         * not confusing
         */
        /*
         * so the loop will only read to k[2] since I put for(int i=0; i< k.length-1;
         * i++)
         */
        k[3] = new Kitchen();
        System.out.println("for customer number 4, *you are special*: ");

        System.out.println("for bread, do you want white or italien?");
        k[3].setBread(scan.next());

        System.out.println("Do you want chicken, meatball or lamb?");
        k[3].setProtein(scan.next());

        System.out.println("Do you want vegetables? yes or no");
        char veggy = Character.toLowerCase(scan.next().charAt(0));
        k[3].setVeggy(veggy);

        System.out.println("Do you want cookies? yes or no");
        char cookieOrNot = Character.toLowerCase(scan.next().charAt(0));
        int cookies = 0;
        if (cookieOrNot == 'y') {
            System.out.println("how many cookies do you want?");
            cookies = scan.nextInt();
        }
        k[3].setCookies(cookies);

        // print results for 4th customer
        System.out.println("Alraight! your fourth order is ready, come pay here: ");

        System.out.println(" ");
        System.out.println("For customer number 4, your order is " + k[3].makeSandwichName());
        System.out.println("Your total is " + String.format("%.2f", k[3].makePrice()) + "$");
        System.out.println("Thank you for your generous tip of " + String.format("%.2f", k[3].generateRandomTip()));

        scan.close();
    }
}
