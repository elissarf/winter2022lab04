import java.util.Random;

public class Kitchen {
    private String bread;
    private String protein;
    private char veggy;
    private int cookies;
    // this is a field for only this class, for usage of 2 methods
    private double price;

    // constructer
    public Kitchen(String bread, String protein, char veggy, int cookies) {
        this.bread = bread;
        this.protein = protein;
        this.veggy = veggy;
        this.cookies = cookies;
    }

    // constructer for forth customer so that it looks neater in the main method
    public Kitchen() {

    }

    // set methods for that forth customer
    public void setBread(String bread) {
        this.bread = bread;
    }

    public void setProtein(String protein) {
        this.protein = protein;
    }

    public void setVeggy(char veggy) {
        this.veggy = veggy;
    }

    public void setCookies(int cookies) {
        this.cookies = cookies;
    }

    // added get methods for lab purpose, is not needed
    public String getBread() {
        return this.bread;
    }

    public String getProtein() {
        return this.protein;
    }

    public char getVeggy() {
        return this.veggy;
    }

    public int getCookies() {
        return this.cookies;
    }

    // method to make a string that will print all the fields
    public String makeSandwichName() {
        String sandwichName = "a " + this.protein + " sandwich with " + this.bread + " bread, ";
        if (veggy == 'n') {
            sandwichName = sandwichName + "no ";
        }
        sandwichName = sandwichName + "vegetables, and " + this.cookies + " cookies.";
        return sandwichName;
    }

    // method to price of the sandwich
    public double makePrice() {
        // make the string fields easier to use by converting to char
        char proteinType = Character.toLowerCase(this.protein.charAt(0));
        char breadType = Character.toLowerCase(this.bread.charAt(0));

        // price for protein
        switch (proteinType) {
            case 'l':
                price = price + 12.49;
                break;
            case 'c':
                price = price + 6.99;
                break;
            case 'm':
                price = price + 8.49;
                break;
        }
        // price for bread
        if (breadType == 'i') {
            price = price + 3.39;
        } else if (breadType == 'w') {
            price = price + 2.19;
        }
        // price for veggy
        if (this.veggy == 'y') {
            price = price + 2.19;
        }
        // price for cookies
        price = price + (this.cookies * 2.49);

        return price;
    }

    // method for making random tip of maximum 50% of the final price
    public double generateRandomTip() {
        Random random = new Random();
        double percetage = random.nextInt(51);
        double tip = percetage / 100 * this.price;
        return tip;
    }

}
